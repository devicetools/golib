#!/bin/bash

for d in $(ls */*.go | sed 's|^\(.*/\).*|\1|' | sort -u); do
    cd ${d}
    go get -v -d
    go build || exit
    cd - >/dev/null
done
