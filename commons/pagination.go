package commons

// Pagination defines the pagination structure for the response
type Pagination struct {
	Limit int `json:"limit"`
	Skip  int `json:"skip"`
	Total int `json:"total"`
}

// GetStartIndex returns the start index for paginating the contents of a slice
func (p Pagination) GetStartIndex() int {
	return Min(p.Skip, p.Total)
}

// GetStopIndex returns the stop index for paginating the contents of a slice
func (p Pagination) GetStopIndex() int {
	return Min(p.Skip+p.Limit, p.Total)
}
