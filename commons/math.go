package commons

// Min returns the minimum between the two given integer arguments
func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
