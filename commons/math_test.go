package commons

import "testing"

// TestMin
func TestMin(t *testing.T) {
	runTestMin(t, 2, 3, 2)
	runTestMin(t, 5, 3, 3)
	runTestMin(t, -2, 1, -2)
	runTestMin(t, -12, -8, -12)
}

// runTestMin runs a test form Min
func runTestMin(t *testing.T, a, b, expected int) {
	result := Min(a, b)
	if result != expected {
		t.Errorf("Min(%v, %v): expected %v but got %v", a, b, expected, result)
	}
}
