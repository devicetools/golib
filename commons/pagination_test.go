package commons

import "testing"

// TestPaginationSlice tests that the pagination of a slice works as expected
func TestPaginationSlice(t *testing.T) {

	// Build the data array to be sliced
	data := make([]int, 20)
	for i := 0; i < 20; i++ {
		data[i] = i
	}

	// No skip and limit smaller than the length of the data
	runTestPagination(t, data, new(0, 10), 0, 9)
	// Skip some and length smaller than the length of the data
	runTestPagination(t, data, new(3, 6), 3, 8)
	// Skip such that the we would go over the length of the slice
	runTestPagination(t, data, new(18, 10), 18, 19)
	// Limit larger than the length of the slice
	runTestPagination(t, data, new(0, 40), 0, 19)
	// Skip larger than the length of the slice
	runTestPagination(t, data, new(22, 5), -1, -1)
}

// new creates a new pagination with the given values
func new(skip, limit int) Pagination {
	return Pagination{
		Limit: limit,
		Skip:  skip,
		Total: 0,
	}
}

// runTestPagination
func runTestPagination(t *testing.T, data []int, p Pagination, firstValue, lastValue int) {
	p.Total = len(data)
	slice := data[p.GetStartIndex():p.GetStopIndex()]

	switch {
	case firstValue == -1:
		if len(slice) != 0 {
			t.Errorf("Expected empty slice for %v, but got %v.", p, slice)
		}
	case slice[0] != firstValue:
		t.Errorf("%v: wrong first value. Expected %v but got %v.", p, firstValue, slice[0])
	case slice[len(slice)-1] != lastValue:
		t.Errorf("%v: wrong last value. Expected %v but got %v.", p, lastValue, slice[len(slice)-1])
	case len(slice) > p.Limit:
		t.Errorf("%v: wrong length of paginated result. Expected maximum %v but got %v", p, p.Limit, len(slice))
	}
}
