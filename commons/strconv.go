package commons

import (
	"fmt"
	"strconv"
)

// Atouint converts the given string to uint
// If the input string is empty, the function returns the zero value for uint (0)
func Atouint(input string) (uint, error) {
	if input == `` {
		return 0, nil
	}

	// Parse to uint64
	result, err := strconv.ParseUint(input, 10, 0)
	if err != nil {
		return 0, err
	}

	// Check that we don't have overflow
	maxUint := ^uint(0)
	if result > uint64(maxUint) {
		return 0, fmt.Errorf(`Value is larger than max uint (%v)`, maxUint)
	}

	return uint(result), nil
}
