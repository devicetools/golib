package commons

import (
	"strconv"
	"testing"
)

// TestAtouint tests the Atouint function
func TestAtouint(t *testing.T) {

	var minUint uint // = 0
	maxUint := ^minUint

	// Cases that should return a valid uint
	runTestAtouint(t, `0`, 0)
	runTestAtouint(t, ``, 0)
	runTestAtouint(t, `1000`, 1000)
	runTestAtouint(t, strconv.FormatUint(uint64(maxUint), 10), maxUint)

	// Cases that should fail
	runTestInvalidAtouint(t, `abcd`)                 // string
	runTestInvalidAtouint(t, `10.23`)                // float
	runTestInvalidAtouint(t, `-46`)                  // negative
	runTestInvalidAtouint(t, `0x46`)                 // hexa (string)
	runTestInvalidAtouint(t, `18446744073709551616`) // too large number
}

// runTestAtouint runs a test form Atouint
func runTestAtouint(t *testing.T, input string, expected uint) {
	result, err := Atouint(input)

	switch {
	case err != nil:
		t.Error(err.Error())
	case result != expected:
		t.Errorf(`Atoui(%v): expected %v but got %v`, input, expected, result)
	}
}

// runTestInvalidAtouint runs a test form Atouint
func runTestInvalidAtouint(t *testing.T, input string) {
	result, err := Atouint(input)
	if err == nil {
		t.Errorf(`Atoui(%v): expected error but got result: %v`, input, result)
	}
}
