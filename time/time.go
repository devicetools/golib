package time

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"time"
)

var nilTime = (time.Time{}).Unix()

// TimeLayout defines the layout used for serializing/deserializing Time
// const TimeLayout string = `2006-01-02T15:04:05`
const TimeLayout string = time.RFC3339

// Time defines our own Time format, based on time.Time, and customized for
// JSON (un)marshalling and DB operations
type Time struct {
	time.Time
}

// Now convenience wrapper for function time.Now
func Now() Time {
	return Time{time.Now()}
}

// Date returns the date in UTC
func (t *Time) Date() Time {
	y, m, d := t.Time.Date()
	return Time{
		Time: time.Date(y, m, d, 0, 0, 0, 0, time.UTC),
	}
}

// ParseDate parses the given date into a Time
func ParseDate(date string) (Time, error) {
	time, err := time.Parse(`2006-01-02`, date)
	return Time{time}, err
}

// UnmarshalJSON overrides the time.UnmarshalJSON
func (t *Time) UnmarshalJSON(b []byte) error {
	var v int64
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}

	t.Time = time.Unix(v, 0)
	return nil
}

// MarshalJSON overrides the time.MarshalJSON
func (t *Time) MarshalJSON() ([]byte, error) {
	if t.Time.Unix() == nilTime {
		return []byte("null"), nil
	}
	return json.Marshal(t.Time.Unix())
}

// IsSet returns true if the Time is neither nil nor nilTime
func (t *Time) IsSet() bool {
	return t != nil && t.Unix() != nilTime
}

// ToTime returns the underlying Time
func (t *Time) ToTime() *time.Time {
	return &t.Time
}

// Scan implements the sql interface for deserializing a Time
func (t *Time) Scan(value interface{}) error {
	var ok bool
	t.Time, ok = value.(time.Time)
	if !ok {
		return fmt.Errorf("value is not of type time.Time")
	}
	return nil
}

// Value implements sql interface for serializing a Time
func (t *Time) Value() (driver.Value, error) {
	if t == nil {
		return nil, nil
	}

	return t.Time.Format(`2006-01-02T15:04:05`), nil
}

// String implements the interface for returning a string representation of Time
func (t *Time) String() string {
	if t == nil {
		return time.Time{}.Format(TimeLayout)
	}

	return t.Time.Format(TimeLayout)
}
