package env

import (
	"log"
	"os"
	"strconv"

	"github.com/kelseyhightower/envconfig"
)

// Config defines the base environment configuration for a DT service
var Config = struct {
	APIPort        string `envconfig:"PORT" default:"80"`
	UIHost         string `envconfig:"UI_HOST"`
	UIPort         string `envconfig:"UI_PORT"`
	DeploymentType string `envconfig:"DEPLOYMENT_TYPE" default:"production"`
}{}

// AuthConfig defines the environment configuration for services that require access control
var AuthConfig = struct {
	AccessToken string `envconfig:"ACCESS_TOKEN" required:"true"`
}{}

// GetAPIPort returns the API port, prefixed by a colon
func GetAPIPort() string {
	return ":" + Config.APIPort
}

// Initialize initializes the Config structure for a given prefix in env
// If auth = TRUE, it also initializes the AuthConfig
func Initialize(prefix string, auth bool) {
	ReadAndValidate(prefix, &Config)
	if auth {
		ReadAndValidate("", &AuthConfig)
	}
}

// GetFromEnv gets the value of the given variable from the environment
// If the variable does not have a value, the function will panic.
func GetFromEnv(variable string) string {
	v := os.Getenv(variable)
	if v == `` {
		log.Fatal("Environment variable '" + variable + "' not found")
	}

	return v
}

// GetIntFromEnv gets the integer value of the given environment variable. If
// the variable is not set, or if its value is not an integer, the function will
// panic
func GetIntFromEnv(variable string) int {
	v, err := strconv.Atoi(GetFromEnv(variable))
	if err != nil {
		panic(err)
	}
	return v
}

// GetBoolFromEnv gets the boolean value of the given environment variable
// If the variable is not set, or if its value is not one of the supported boolean
// strings, the function will panic
func GetBoolFromEnv(variable string) bool {
	v, err := strconv.ParseBool(GetFromEnv(variable))
	if err != nil {
		panic(err)
	}
	return v
}

// GetTokenLifeTime reads the life time for the jwt access token from the environment
func GetTokenLifeTime(prefix string) int64 {
	cfg := struct {
		Value int64 `envconfig:"TOKEN_LIFETIME" required:"true"`
	}{}

	ReadAndValidate(prefix, &cfg)
	return cfg.Value
}

// GetJwtSigningKey reads the JWT signing key from the environment
func GetJwtSigningKey(prefix string) string {
	// cfg := struct {
	// 	SigningKey string `envconfig:"SIGNING_KEY" required:"true"`
	// }{}
	//
	return GetFromEnv(`SIGNING_KEY`)
	//
	// ReadAndValidate(prefix, &cfg)
	// return cfg.SigningKey
}

// ReadAndValidate initializes the given config from environment and validates it,
// throwing an error if the required varialbe is not defined
func ReadAndValidate(prefix string, cfg interface{}) {
	if err := envconfig.Process(prefix, cfg); err != nil {
		log.Fatal(err)
	}
}
