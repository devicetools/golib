package api

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/devicetools/golib/env"
)

// ValidateAccessToken checks whether request contains a valid access
// token. The value of the expected access token is stored in
// env.Config.AccessToken and must be explicitely initialized by the client,
// by calling env.Initialize().
func ValidateAccessToken(h http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("Authorization")
		if token == env.AuthConfig.AccessToken {
			h.ServeHTTP(res, req)
		} else {
			msg := "Authorization token mismatch"
			if token == "" {
				msg = "Requires authorization"
			}
			res.WriteHeader(http.StatusUnauthorized)
			_ = json.NewEncoder(res).Encode(clientError{Message: msg}) // We don't check the error
		}
	})
}
