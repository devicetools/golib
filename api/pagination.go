package api

import (
	"encoding/json"
	"net/http"
	"reflect"
	"strconv"

	"github.com/jinzhu/gorm"

	"bitbucket.org/devicetools/golib/commons"
)

// DefaultPagination returns the default pagination
func DefaultPagination() commons.Pagination {
	return commons.Pagination{
		Limit: 20,
		Skip:  0,
		Total: 0,
	}
}

// ParsePaginationRequest parses the arguments for a pagination request from the
// given HTTP request. If the pagination request is malformed, the default
// PaginationRequest is returned.
func ParsePaginationRequest(r *http.Request) commons.Pagination {
	result := DefaultPagination()

	// Set the limit
	if limit := GetFromQuery(r, "limit"); limit != `` {
		if l, err := strconv.Atoi(limit); err == nil {
			result.Limit = l
		}
	}

	// Set the skip
	if skip := GetFromQuery(r, "skip"); skip != `` {
		result.Skip, _ = strconv.Atoi(skip)
	}

	// Set the total
	if total := GetFromQuery(r, "total"); total != `` {
		result.Total, _ = strconv.Atoi(total)
	}
	return result
}

// GetPaginationRequest wraps ParsePaginationRequest, and also fills in the
// Pagination.Total field (if its value is 0), by querying the given database
// with the given model.
func GetPaginationRequest(r *http.Request, db *gorm.DB, model interface{}) commons.Pagination {
	result := ParsePaginationRequest(r)

	// Set the total, based on the given db and model
	if result.Total == 0 {
		db.Model(model).Count(&(result.Total))
	}

	return result
}

// Collection a results collection that contains items and pagination information
// This should be used if you return a multiple items in a REST API
type Collection struct {
	commons.Pagination
	Items []interface{} `json:"items"`
}

// MarshalJSON implements JSON marshaling for a Collection
func (c Collection) MarshalJSON() ([]byte, error) {
	type ResultsCollection Collection
	return json.Marshal(&struct {
		Results ResultsCollection `json:"results"`
	}{
		Results: (ResultsCollection)(c),
	})
}

// WrapCollection wraps a given collection for pagination
func WrapCollection(items interface{}, requested commons.Pagination) Collection {
	slice := toInterfaceSlice(items)
	return Collection{
		Pagination: requested,
		Items:      slice,
	}
}

func toInterfaceSlice(input interface{}) []interface{} {
	if reflect.TypeOf(input).Kind() == reflect.Slice {
		slice := reflect.ValueOf(input)
		output := make([]interface{}, slice.Len())
		for i := 0; i < slice.Len(); i++ {
			output[i] = slice.Index(i).Interface()
		}
		return output
	}

	return []interface{}{input}
}
