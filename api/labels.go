package api

// This file contains the standard labels and URL paths that are used in the REST API

/* We need to allow account ids to also contain letters, in order to support
 * the existing accounts (e.g. swiss)
 */
const AccountID = `account_id`
const AccountPath = `/accounts/{` + AccountID + `:[a-z0-9]+}`

const AppID = `app_id`
const AppPath = `/apps/{` + AppID + `:[a-z0-9]+}`

const BundleID = `bundle_id`
const BundlePath = `/bundle/{` + BundleID + `:[0-9]+}`

const ServiceID = `service_id`
const ServicePath = `/services/{` + ServiceID + `:[a-z0-9]+}`

const ProviderID = `provider_id`
const ProvidersPath = `/providers/{` + ProviderID + `:[A-Z0-9]+}`

const UserID = `user_id`
const UserPath = `/users/{` + UserID + `:[a-z0-9]+}`

const RegionPath = `"/regions/{region}`
