package api

import (
	"log"
	"net/http"

	"github.com/kelseyhightower/envconfig"
	"github.com/yvasiyarov/gorelic"
)

type NewRelicConfig struct {
	LicenceKey string `envconfig:"LICENSE_KEY"`
	Name       string
}

// A HTTP tracking agent that does nothing
type NoAgent struct{}

func (a NoAgent) TrackRequest(h http.Handler) http.Handler {
	return h
}

// Collect statistics about HTTP requests
type HttpAgent interface {
	TrackRequest(h http.Handler) http.Handler
}

type NewRelicHttpAgent struct {
	agent *gorelic.Agent
}

// A wrapper around the NewRelic HTTP agent to only expose HTTP tracking
func (a NewRelicHttpAgent) TrackRequest(h http.Handler) http.Handler {
	return a.agent.WrapHTTPHandler(h)
}

func (a NewRelicHttpAgent) Run() error {
	return a.agent.Run()
}

func NewRelicAgentFromConfig() NewRelicHttpAgent {
	var cfg NewRelicConfig
	err := envconfig.Process("newrelic", &cfg)
	if err != nil {
		log.Fatal(err)
	}

	agent := gorelic.NewAgent()
	agent.CollectHTTPStat = true
	agent.CollectHTTPStatuses = true
	agent.NewrelicLicense = cfg.LicenceKey
	agent.NewrelicName = cfg.Name

	return NewRelicHttpAgent{agent}
}
