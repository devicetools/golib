package api

import (
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// ContextKey type for the keys to be used for adding/retrieving context values to the HTTP handler context
type ContextKey string

const (
	// ScopesKey the key to be used for adding/retrieving scopes
	ScopesKey ContextKey = `scopes`
	// DeviceIDKey the key to be used for adding/retrieving device id
	DeviceIDKey ContextKey = `device_id`
	// SessionToken the key to be used for adding/retrieving device id
	SessionToken ContextKey = `session_token`

	UserId ContextKey = `user_id`
)

// Scope interface for operations on scopes
type Scope interface {
	HasScope(string) bool
}

// ScopeClaims type for the scope claims in the JWT token
type ScopeClaims struct {
	Scopes []string `json:"scopes"`
	// Recommended having
	jwt.StandardClaims
}

// SetIssueTime implementes the SignableClaims interface
func (c *ScopeClaims) SetIssueTime() {
	c.IssuedAt = time.Now().Unix()
}

// SetExpiration implementes the SignableClaims interface
func (c *ScopeClaims) SetExpiration(expirationTime int64) {
	c.ExpiresAt = expirationTime
}

// Get implements the SignableClaims interface
func (c ScopeClaims) Get() jwt.Claims {
	return c
}

// Scopes type for scopes
type scopes map[string]bool

// HasScope returns TRUE if the scopes contains the given scope
func (s scopes) HasScope(scope string) bool {
	_, ok := s[scope]
	return ok
}

// ExtractScopes extracts the scopes from the context of the HTTP request into a slice of strings
func ExtractScopes(r *http.Request) Scope {
	// Extract the scopes as interface
	scopesInterface := r.Context().Value(ScopesKey)
	if scopesInterface == nil {
		return nil
	}

	activeScopes, _ := scopesInterface.([]string)
	result := make(scopes, len(activeScopes))
	for _, s := range activeScopes {
		result[s] = true
	}

	return result
}

// CheckScope returns error if the HTTP request does not contain the given scope.
func CheckScope(r *http.Request, scope string) error {
	scopes := ExtractScopes(r)
	if scopes == nil {
		return ForbiddenError("No scopes provided in JWT")
	}

	if scopes.HasScope(scope) {
		return nil
	}

	return ForbiddenError("Device may not do " + scope + " operations")
}
