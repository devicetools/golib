package api

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Error represents a handler error. It provides methods for a HTTP status
// code and embeds the built-in error interface.
type ApiError interface {
	error
	Status() int
}

// StatusError represents an error with an associated HTTP status code.
type StatusError struct {
	Code int
	Err  error
}

// DetailedError represents an error with additional information
type DetailedError struct {
	Code    int
	Err     error
	Details interface{}
}

// A struct to hold the format for returning an error message
type clientError struct {
	Message string      `json:"error"`
	Details interface{} `json:"details,omitempty"`
}

// Status Returns our HTTP status code.
func (se StatusError) Status() int {
	return se.Code
}

// Allows StatusError to satisfy the error interface.
func (se StatusError) Error() string {
	return se.Err.Error()
}

// Status Returns our HTTP status code.
func (de DetailedError) Status() int {
	return de.Code
}

// Allows DetailedError to satisfy the error interface.
func (de DetailedError) Error() string {
	return de.Err.Error()
}

// BadRequestError returns a 400 BAD REQUEST status error
func BadRequestError(msg string, args ...interface{}) StatusError {
	return StatusError{Code: http.StatusBadRequest, Err: fmt.Errorf(msg, args...)}
}

// UnauthorizedError returns a 401 UNAUTHORIZED error
func UnauthorizedError(msg string, args ...interface{}) StatusError {
	return StatusError{Code: http.StatusUnauthorized, Err: fmt.Errorf(msg, args...)}
}

// ForbiddenError returns a 403 FORBIDDEN error
func ForbiddenError(msg string, args ...interface{}) StatusError {
	return StatusError{Code: http.StatusForbidden, Err: fmt.Errorf(msg, args...)}
}

// NotFoundError returns a 404 NOT FOUND error
func NotFoundError(msg string, args ...interface{}) StatusError {
	return StatusError{Code: http.StatusNotFound, Err: fmt.Errorf(msg, args...)}
}

// NotAcceptableError returns a 406 NOT ACCEPTABLE error
func NotAcceptableError(msg string, args ...interface{}) StatusError {
	return StatusError{Code: http.StatusNotAcceptable, Err: fmt.Errorf(msg, args...)}
}

// PreconditionFailedError returns a 412 PRECONDITION FAILED status error
func PreconditionFailedError(msg string, args ...interface{}) StatusError {
	return StatusError{Code: http.StatusPreconditionFailed, Err: fmt.Errorf(msg, args...)}
}

// UnprocessableEntityError returns a 422 UNPROCESSABLE ENTITY status error
func UnprocessableEntityError(msg string, args ...interface{}) StatusError {
	return StatusError{Code: http.StatusUnprocessableEntity, Err: fmt.Errorf(msg, args...)}
}

// StatusRequestHeaderFieldsTooLarge returns a 431 REQUEST HEADER FIELDS TOO LARGE error
func StatusRequestHeaderFieldsTooLarge(msg string, args ...interface{}) StatusError {
	return StatusError{Code: http.StatusRequestHeaderFieldsTooLarge, Err: fmt.Errorf(msg, args...)}
}

// InternalServerError returns a 500 INTERNAL SERVER ERROR
func InternalServerError(msg string, args ...interface{}) StatusError {
	return StatusError{Code: http.StatusInternalServerError, Err: fmt.Errorf(msg, args...)}
}

// ErrorHTTPHandler error aware HTTP handler
type ErrorHTTPHandler func(w http.ResponseWriter, r *http.Request) error

// ErrorHandler supports returning ApiError objects and serializes JSON
// containing the error message
func ErrorHandler(h ErrorHTTPHandler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := h(w, r)

		if err != nil {
			w.Header().Set(`Content-Type`, `application/json`)
			switch e := err.(type) {
			case DetailedError:
				w.WriteHeader(e.Code)
				json.NewEncoder(w).Encode(clientError{Message: err.Error(), Details: e.Details})
			case ApiError:
				w.WriteHeader(e.Status())
				json.NewEncoder(w).Encode(clientError{Message: err.Error()})
			default:
				// Any error types we don't specifically look out for default
				// to serving a HTTP 500
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(clientError{Message: err.Error()})
			}
		}
	})
}
