package api

import (
	"encoding/json"
	"net/http"
)

// Deal with not found routes and return message to look up docs
func NotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	msg := `No API handler matched your request.
	You might be using the wrong resource paths.
	Please consult the API documentation for examples
	and coverage of the supported methods.`
	json.NewEncoder(w).Encode(clientError{Message: msg})
}
