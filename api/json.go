package api

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"reflect"

	"github.com/asaskevich/govalidator"
)

// JSONHandler set JSON content type for request, if nothing already set
func JSONHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Don't overwrite the Content-Type, if already set
		key := `Content-Type`
		if w.Header().Get(key) != `` {
			return
		}

		s := r.URL.String()
		switch {
		case s == `/doc`:
			// Do nothing
		default:
			w.Header().Set(key, `application/json`)
		}

		h.ServeHTTP(w, r)
	})
}

// DecodeJSON decodes the JSON in the HTML request body into the given body reference.
// If the decoding fails, the function returns a UnprocessableEntityError StatusError.
func DecodeJSON(r io.Reader, body interface{}) error {
	// Try to decode the body to the specified structure
	if err := json.NewDecoder(r).Decode(&body); err != nil {
		return UnprocessableEntityError(err.Error())
	}

	return nil
}

// DecodeAndValidateJSON decodes the JSON in the HTML request body via DecodeJSON.
// If the decoding succeeds, the function validates the resulting structure (if
// requested) and returns PreconditionFailedError, if the validation has failed
func DecodeAndValidateJSON(r io.Reader, body interface{}) error {
	val := reflect.ValueOf(body)
	if val.Kind() != reflect.Ptr {
		return fmt.Errorf("expected body to be a pointer. got %s", val.Kind())
	}

	// Try to decode the body to the specified structure
	if err := DecodeJSON(r, &body); err != nil {
		return UnprocessableEntityError(err.Error())
	}

	// Validate the resulting structure
	if _, err := govalidator.ValidateStruct(body); err != nil {
		return PreconditionFailedError(err.Error())
	}

	return nil
}
