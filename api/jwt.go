package api

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// SignableClaims interface for working with signable claims
type SignableClaims interface {
	SetIssueTime()
	SetExpiration(expirationTime int64)
	Get() jwt.Claims
}

// JwtTokenFromHeader Extract JWT token from the Authorization header.
func JwtTokenFromHeader(r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return "", UnauthorizedError("No Authorization header provided")
	}

	authHeaderParts := strings.Split(authHeader, " ")
	if len(authHeaderParts) != 2 || strings.ToLower(authHeaderParts[0]) != "bearer" {
		err := fmt.Errorf("Authorization header format must be Bearer {token}")
		return "", UnauthorizedError(err.Error())
	}

	return authHeaderParts[1], nil
}

// BuildJwtToken creates a JWT token using the provided signing key
func BuildJwtToken(claims SignableClaims, key string, lifeTime int64) string {
	// Set the issue time
	claims.SetIssueTime()

	// ... and the expiration time, if non-zero
	if lifeTime > 0 {
		claims.SetExpiration(time.Now().Unix() + lifeTime)
	}

	// Build the token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims.Get())
	// ... sign...
	signedToken, _ := token.SignedString([]byte(key))
	// ... and return it
	return signedToken
}

// ExtractJwtToken returns a jwt token, after validating it
func ExtractJwtToken(req *http.Request, key string, claims jwt.Claims) (*jwt.Token, error) {
	tokenValue, err := JwtTokenFromHeader(req)
	if err != nil {
		return nil, err
	}

	return jwt.ParseWithClaims(tokenValue, claims,
		func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method")
			}
			return []byte(key), nil
		})
}

// ValidateToken set JSON content type for request
func ValidateToken(h http.Handler, key string, checkExpiry bool) http.Handler {
	return ErrorHandler(func(res http.ResponseWriter, req *http.Request) error {

		// We need to parse the token ourselves (instead of using ExtractJwtToken)
		// because we don't want to error if the token is expired
		tokenValue, err := JwtTokenFromHeader(req)
		if err != nil {
			return err
		}

		token, err := jwt.ParseWithClaims(tokenValue, &ScopeClaims{},
			func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, err
				}
				return []byte(key), nil
			})

		// If there is an error, and the token is nill or the token is valid, we freak-out
		if err != nil && (token == nil || token.Valid) {
			return ForbiddenError(err.Error())
		}

		if !token.Valid {
			if ve, ok := err.(*jwt.ValidationError); ok {
				// We freak-out:
				//  - always if checkExpiry is true OR
				//  - if the error is not ValidationErrorExpired
				if checkExpiry || (ve.Errors&jwt.ValidationErrorExpired == 0) {
					return UnauthorizedError(err.Error())
				}
			} else {
				panic(errors.New("Could not handle error: " + err.Error()))
			}
		}

		// Grab the tokens claims and pass it into the original request
		if claims, ok := token.Claims.(*ScopeClaims); ok {
			// Add the decoded device_id
			scopesCtx := context.WithValue(req.Context(), DeviceIDKey, claims.Id)
			// Add the decoded scopes
			scopesCtx = context.WithValue(scopesCtx, ScopesKey, claims.Scopes)

			h.ServeHTTP(res, req.WithContext(scopesCtx))
		} else {
			return UnauthorizedError("Token is not valid")
		}

		return nil
	})
}

// ValidateToken set JSON content type for request
func ValidateTokenWithSession(h http.Handler, key string, checkExpiry bool) http.Handler {
	return ErrorHandler(func(res http.ResponseWriter, req *http.Request) error {

		// We need to parse the token ourselves (instead of using ExtractJwtToken)
		// because we don't want to error if the token is expired
		tokenValue, err := JwtTokenFromHeader(req)
		if err != nil {
			return err
		}
		token, err := jwt.ParseWithClaims(tokenValue, &SessionClaims{},
			func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, err
				}
				return []byte(key), nil
			})

		// If there is an error, and the token is nill or the token is valid, we freak-out
		if err != nil && (token == nil || token.Valid) {
			return ForbiddenError(err.Error())
		}

		if !token.Valid {
			if ve, ok := err.(*jwt.ValidationError); ok {
				// We freak-out:
				//  - always if checkExpiry is true OR
				//  - if the error is not ValidationErrorExpired
				if checkExpiry || (ve.Errors&jwt.ValidationErrorExpired == 0) {
					return UnauthorizedError(err.Error())
				}
			} else {
				panic(errors.New("Could not handle error: " + err.Error()))
			}
		}

		// Grab the tokens claims and pass it into the original request
		if claims, ok := token.Claims.(*SessionClaims); ok {
			// Add the decoded device_id
			scopesCtx := context.WithValue(req.Context(), DeviceIDKey, claims.Id)
			// Add the decoded device_id
			scopesCtx = context.WithValue(scopesCtx, SessionToken, claims.SessionToken)
			// Add the decoded device_id
			scopesCtx = context.WithValue(scopesCtx, UserId, claims.UserID)
			// Add the decoded scopes
			scopesCtx = context.WithValue(scopesCtx, ScopesKey, claims.Scopes)
			h.ServeHTTP(res, req.WithContext(scopesCtx))
		} else {
			return UnauthorizedError("Token is not valid")
		}

		return nil
	})
}

// GetDeviceID extracts the device ID from the HTTP request
// This function is intended to be used on HTTP requests that contain the JWT
// token, after the token was validated by calling ValidateToken
func GetDeviceID(r *http.Request) (string, error) {
	id := r.Context().Value(DeviceIDKey).(string)
	if id == `` {
		return ``, BadRequestError("No device ID found in the JWT access token")
	}
	return id, nil
}

// GetSessionToken extracts the device ID from the HTTP request
// This function is intended to be used on HTTP requests that contain the JWT
// token, after the token was validated by calling ValidateToken
func GetSessionToken(r *http.Request) (string, error) {
	const (
		// ScopesKey the key to be used for adding/retrieving scopes
		// DeviceIDKey the key to be used for adding/retrieving device id

		SessionToken ContextKey = `sessiontoken`
	)
	token := r.Context().Value(SessionToken)
	if token == nil || token.(string) != `` {
		return ``, BadRequestError("No session token found on the JWT")
	}
	return token.(string), nil
}

// GetUserID extracts the device ID from the HTTP request
// This function is intended to be used on HTTP requests that contain the JWT
// token, after the token was validated by calling ValidateToken
func GetUserID(r *http.Request) (uint, error) {

	const (
		// ScopesKey the key to be used for adding/retrieving scopes
		// DeviceIDKey the key to be used for adding/retrieving device id

		UserID ContextKey = `user_id`
	)
	token := r.Context().Value(UserID)

	if token == nil {
		return 0, BadRequestError("No user id found on the JWT")
	}
	return token.(uint), nil
}
