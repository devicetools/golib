package api

import (
	"io/ioutil"
	"net/http"
)

// SendDocFile sends the contents of the given doc file to the HTTP ResponseWriter
func SendDocFile(w http.ResponseWriter, fileName string) error {
	dat, err := ioutil.ReadFile(fileName)
	if err == nil {
		w.Header().Set("Content-Type", "text/html")
		_, _ = w.Write(dat) // we don't check the error message, because there's nothing we can do about it
	}

	return err
}
