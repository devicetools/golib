package api

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/kelseyhightower/envconfig"
	"github.com/stvp/rollbar"
)

type RollbarConfig struct {
	Token       string
	Environment string `default:"development"`
}

func InitRollbarFromConfig() {
	var cfg RollbarConfig
	err := envconfig.Process("rollbar", &cfg)
	if err != nil {
		log.Fatal(err)
	}

	rollbar.Token = cfg.Token
	rollbar.Environment = cfg.Environment
}

func RollbarRecoveryHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				if errr, ok := err.(error); ok {
					fmt.Fprintln(os.Stderr, errr.Error())
					rollbar.RequestError(rollbar.ERR, r, errr)
				} else {
					rollbar.RequestError(rollbar.ERR, r, errors.New(fmt.Sprintf("%v", err)))
				}
				w.WriteHeader(http.StatusInternalServerError)
			}
		}()
		h.ServeHTTP(w, r)
	})
}
