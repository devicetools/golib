package api

import (
	"errors"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type SessionClaims struct {
	UserID       uint     `json:"userID"`
	SessionToken string   `json:"sessiontoken"`
	Scopes       []string `json:"scopes"`
	// Recommended having
	jwt.StandardClaims
}

func (c *SessionClaims) SetIssueTime() {
	c.IssuedAt = time.Now().Unix()
}

func (c *SessionClaims) SetExpiration(expirationTime int64) {
	c.ExpiresAt = expirationTime
}

func (c SessionClaims) Get() jwt.Claims {
	return c
}

// ExtractScopes extracts the scopes from the context of the HTTP request into a slice of strings
func ExtractSession(r *http.Request) (string, error) {
	// Extract the scopes as interface
	const (
		SessionToken ContextKey = `session_token`
	)
	scopesInterface := r.Context().Value(SessionToken)
	if scopesInterface == nil {
		return "", errors.New("no session")
	}

	return scopesInterface.(string), nil
}
