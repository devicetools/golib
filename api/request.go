package api

import (
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/devicetools/golib/commons"
	golibtime "bitbucket.org/devicetools/golib/time"
	"github.com/gorilla/mux"
)

// GetFromHeader extracts the provider from the http request
// If the header doesn't contain the label, the function returns a BadRequest
// StatusError
func GetFromHeader(r *http.Request, label string) (string, error) {
	result := r.Header.Get(label)
	if result == "" {
		return "", BadRequestError("Bad header: missing required argument '" + label + "'")
	}

	return result, nil
}

// GetFromQuery extracts from the query string the value for the given label
// If the query doesn't contain the label, the function returns an empty string
func GetFromQuery(r *http.Request, label string) string {
	return r.URL.Query().Get(label)
}

// GetSliceFromQuery extracts a slice of values from the query string for the
// given label. If the query doesn't contain the label, the function returns an
// empty slice. The multiple values are specified in the request by multiple
// occurences of the same label.
func GetSliceFromQuery(r *http.Request, label string) []string {
	return r.URL.Query()[label]
}

// GetDateFromQuery extracts a date from the query and returns it as a
// golibtime.Time. If the query string cannot be parsed into a Time, the function
// returns a non-nil error.
func GetDateFromQuery(r *http.Request, tag string, defaultTime time.Time) (golibtime.Time, error) {
	date := GetFromQuery(r, tag)
	if date == `` {
		return golibtime.Time{Time: defaultTime}, nil
	}

	return golibtime.ParseDate(date)
}

// GetBoolFromQuery extracts from the query string the boolean value for the
// given label. If the query doesn't contain the label, the function returns
// FALSE.
func GetBoolFromQuery(r *http.Request, label string) (bool, error) {
	value := GetFromQuery(r, label)
	if value == `` {
		return false, nil
	}
	return strconv.ParseBool(value)
}

// GetFromURL extracts from the URI the value for the given label. If the URL
// doesn't contain the label, the function returns BadRequest StatusError.
func GetFromURL(r *http.Request, label string) (string, error) {
	result, ok := mux.Vars(r)[label]
	if !ok {
		return "", BadRequestError("Bad URI: missing required component '" + label + "'")
	}

	return result, nil
}

// GetUintFromURL extracts from the URI the value for the given label and
// converts it to uint. If the URL doesn't contain the label, the function returns
// BadRequest StatusError
func GetUintFromURL(r *http.Request, label string) (uint, error) {
	resultString, err := GetFromURL(r, label)
	if err != nil {
		return 0, err
	}

	result, err := commons.Atouint(resultString)
	if err != nil {
		return 0, UnprocessableEntityError(err.Error())
	}

	return result, nil
}

// GetRequestProtocol returns the protocol used for http request (http or https)
func GetRequestProtocol(r *http.Request) string {
	// If running behind traefik, check for X-Forwarded-Proto header first
	xForwardedHeader := r.Header.Get("X-Forwarded-Proto")
	if xForwardedHeader != `` {
		return xForwardedHeader
	} else if r.TLS != nil {
		return `https`
	}

	return `http`
}

// GetBaseURL builds the request URL (protocol://baseUr) from the given http request
func GetBaseURL(r *http.Request) string {
	return GetRequestProtocol(r) + `://` + r.Host
}
