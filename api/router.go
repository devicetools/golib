package api

import (
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/devicetools/golib/env"

	"github.com/gorilla/mux"
)

// NoopInitializer HTTP handler function that does nothing. This function is
// provided as convenience, to be used as input arguments to NewRouter by
// clients of the library that don't require special initialization for testing.
func NoopInitializer(w http.ResponseWriter, r *http.Request) {
}

// NewRouter sets the standard doc endpoints (/doc and /changelog) to the given
// router. Optionally, if the UI_HOST and UI_PORT are set in the environmet, it
// also sets the /ui endpoint.
// Starting with version 3.0.0, the function requires the initializerForTesting
// argument, which is a function handle that should perform initialization of
// the environment for acceptance testing.
func NewRouter(initializerForTesting http.HandlerFunc) *mux.Router {
	if initializerForTesting == nil {
		panic(`golib.NewRouter: Initializer function handler should not be nil`)
	}

	router := mux.NewRouter().StrictSlash(true)
	router.NotFoundHandler = http.HandlerFunc(NotFound)

	// Function tries to open the file and panics if it can't
	ensureFileExists := func(fileName string) string {
		if _, err := os.Stat(fileName); err != nil {
			panic(err)
		}
		return fileName
	}

	// API Doc
	apidocFile := ensureFileExists(`doc/apidoc.html`)
	router.Path("/doc").Methods(http.MethodGet).Handler(ErrorHandler(
		func(w http.ResponseWriter, r *http.Request) error {
			return SendDocFile(w, apidocFile)
		}))

	// CHANGELOG
	changelogFile := ensureFileExists(`doc/CHANGELOG.html`)
	router.Path("/changelog").Methods(http.MethodGet).Handler(ErrorHandler(
		func(w http.ResponseWriter, r *http.Request) error {
			return SendDocFile(w, changelogFile)
		}))

	// Setup the test environment - expose it only for testing
	if env.Config.DeploymentType == "testing" {
		router.PathPrefix("/initdb").Subrouter().Methods(http.MethodPost).HandlerFunc(initializerForTesting)
	}

	return router
}

// StartServer starts the standard API server, performing some sanity checks along
// the way
func StartServer(r http.Handler, readTimeout, writeTimeout time.Duration) error {
	port := env.GetAPIPort()
	if port == `` {
		log.Fatal("Server PORT not defined. This is probably because the function golib.env.Initialize() is not called during intialization")
	}

	srv := &http.Server{
		Handler:      r,
		Addr:         port,
		WriteTimeout: writeTimeout * time.Second,
		ReadTimeout:  readTimeout * time.Second,
	}

	return srv.ListenAndServe()
}
