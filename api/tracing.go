package api

import (
	"bufio"
	"context"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"
)

type key int

const (
	requestIDKey key = 0
	counterKey   key = 1
)

// TraceLogger defines a trace logger
type TraceLogger interface {
	Log(r *http.Request, format string, a ...interface{})
	Close()
}

// ConsoleLogger defines a console logger
type ConsoleLogger struct {
}

// Log implements TraceLogger interface
func (logger ConsoleLogger) Log(r *http.Request, format string, a ...interface{}) {
	fmt.Printf("%v    %v    ", r.Context().Value(requestIDKey), r.Context().Value(counterKey))
	fmt.Printf(format+"\n", a...)
}

// Close Implements TraceLogger interface
func (logger ConsoleLogger) Close() {
	// Do nothing
}

// FileLogger defines a file logger
type FileLogger struct {
	Writer *bufio.Writer
}

// NewFileLogger attempts to create a FileLogger and panics if it fails
func NewFileLogger(fileName string) FileLogger {
	f, err := os.Create(fileName)
	if err != nil {
		panic(err.Error())
	}

	return FileLogger{Writer: bufio.NewWriter(f)}
}

// Log implements TraceLogger interface
func (logger FileLogger) Log(r *http.Request, format string, a ...interface{}) {
	fmt.Fprintf(logger.Writer, "%v    %v    ", r.Context().Value(requestIDKey), r.Context().Value(counterKey))
	fmt.Fprintf(logger.Writer, format+"\n", a...)
	_ = logger.Writer.Flush()
}

// Close implements TraceLogger interface
// Note: This won't work in case the program is killed, unless we catch the kill signal in the server
func (logger FileLogger) Close() {
	fmt.Println(`Flusing!!!`)
	_ = logger.Writer.Flush() // Not much we can do if error
}

// HandlerWithTrace defines a
type HandlerWithTrace func(ctx *TraceLogger, w http.ResponseWriter, r *http.Request)

// TracingHandler handles tracing
func TracingHandler() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			// Handle the request ID
			requestID := r.Header.Get("X-Request-Id")
			isFirstRequest := (requestID == "")
			if isFirstRequest {
				requestID = fmt.Sprintf("%v", time.Now().UnixNano())
			}
			ctx := context.WithValue(r.Context(), requestIDKey, requestID)

			// Handle the counter
			counter := 1
			counterString := r.Header.Get("X-Counter")
			if counterString != "" {
				val, err := strconv.Atoi(counterString)
				if err != nil {
					w.Header().Set(`X-Tracing-Error`, fmt.Sprintf("Invalid counter value: %+v", counterString))
					if isFirstRequest {
						val = 0
					} else {
						val = 1
					}
				}
				counter = val + 1
			}
			counterString = fmt.Sprintf("%+v", counter)
			ctx = context.WithValue(ctx, counterKey, counterString)

			w.Header().Set("X-Request-Id", requestID)
			w.Header().Set("X-Counter", counterString)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
