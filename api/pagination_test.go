package api

import (
	"net/http"
	"net/url"
	"testing"

	"bitbucket.org/devicetools/golib/commons"
)

// TestParsePaginationRequest
func TestParsePaginationRequest(t *testing.T) {
	runTestPagination(t, ``, ``, ``, DefaultPagination())
	runTestPagination(t, `3`, ``, `8`, commons.Pagination{Limit: 3, Skip: 0, Total: 8})
	runTestPagination(t, `3`, `4`, ``, commons.Pagination{Limit: 3, Skip: 4, Total: 0})
	runTestPagination(t, `73`, `28`, `14`, commons.Pagination{Limit: 73, Skip: 28, Total: 14})
}

// runTestPagination runs a test form ParsePaginationRequest
func runTestPagination(t *testing.T, limit, skip, total string, expected commons.Pagination) {
	req, err := http.NewRequest("Parse", "/pag_test", nil)
	if err != nil {
		t.Fatal(err)
	}

	q := url.Values{}
	q.Add("limit", limit)
	q.Add("skip", skip)
	q.Add("total", total)
	req.URL.RawQuery = q.Encode()

	result := ParsePaginationRequest(req)
	if result != expected {
		t.Errorf("ParsePaginationRequest with limit = %v, skip = %v, total = %v): expected %v but got %v", limit, skip, total, expected, result)
	}
}
