package mail

import (
	"log"

	"bitbucket.org/devicetools/golib/env"

	"gopkg.in/mailgun/mailgun-go.v1"
)

// MailgunSender can be used as stand-alone object or as subclass for more specialized
// e-mail sender interfaces
type MailgunSender struct {
	client      mailgun.Mailgun
	senderEmail string
}

type mailgunConfig struct {
	Domain       string `required:"true"`
	APIKey       string `required:"true" envconfig:"API_KEY"`
	PublicAPIKey string `required:"true" envconfig:"PUBLIC_API_KEY"`
	Sender       string `default:"postmaster@mailgun.devicetools.ch"`
}

// New creates a new MailgunSender object
// It is recommended to create this object at the start of the program and
// pass it as an argument to subsequent function calls, instead of creating it
// anew every time an e-mail has to be sent
//
// MAILGUN_DOMAIN
// MAILGUN_API_KEY
// MAILGUN_PUBLIC_API_KEY
//
func New() MailgunSender {
	var cfg mailgunConfig
	env.ReadAndValidate("mailgun", &cfg)

	return MailgunSender{
		client:      mailgun.NewMailgun(cfg.Domain, cfg.APIKey, cfg.PublicAPIKey),
		senderEmail: cfg.Sender,
	}
}

// Send sends an e-mail with the given subjet and body to the given recipient(s)
// Additionaly, the function sets the tracking of the message to the specified value
func (s MailgunSender) Send(subject, body string, withTracking bool, recipients ...string) {
	msg := s.client.NewMessage(s.senderEmail, subject, body, recipients...)
	msg.SetHtml(body)
	msg.SetTracking(withTracking)

	_, _, err := s.client.Send(msg)
	if err != nil {
		log.Fatal(err)
	}
}
