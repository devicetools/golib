package translations

// --
// -- Table structure for table `languages`
// --
// CREATE TABLE `languages` (
//   `id` varchar(10) NOT NULL PRIMARY KEY
// ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

import (
	"strings"

	"github.com/jinzhu/gorm"
)

// Language defines a supported language
type Language struct {
	ID string `json:"id"`
}

// IsSupportedLanguage returns true if the given string is a supported language
func IsSupportedLanguage(db *gorm.DB, language string) bool {
	allLanguages := getLanguages(db)
	key := languageKey(language)
	for _, l := range allLanguages {
		if l.ID == key {
			return true
		}
	}

	return false
}

// SupportedLanguages returns a list of supported languages
func SupportedLanguages(db *gorm.DB) []string {
	allLanguages := getLanguages(db)

	result := make([]string, len(allLanguages))
	for i, k := range allLanguages {
		result[i] = k.ID
	}

	return result
}

// AddLanguage adds a new supported language
func AddLanguage(db *gorm.DB, language string) error {
	key := languageKey(language)

	// Add the new language to the DB
	newLanguage := Language{ID: key}
	return db.Create(&newLanguage).Error
}

func languageKey(l string) string {
	return strings.ToUpper(l)
}

func getLanguages(db *gorm.DB) []Language {
	var languages []Language
	if err := db.Find(&languages).Error; err != nil && err != gorm.ErrRecordNotFound {
		panic(err)
	}

	return languages
}
