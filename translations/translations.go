package translations

import "github.com/jinzhu/gorm"

// --
// -- Table structure for table `translations`
// --
// CREATE TABLE `XXXXXXXXXXXX_translations` (
//   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//   `language` varchar(10) NOT NULL,
//   `body` varchar(127) NOT NULL,
//   PRIMARY KEY (`id`, `language`),
//   KEY `fk_translation_language` (`language`),
//   CONSTRAINT `fk_translation_language` FOREIGN KEY (`language`) REFERENCES `languages` (`id`)
// ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

// Translation defines a translation
type Translation struct {
	ID       uint   `json:"-" gorm:"primary_key"`
	Language string `json:"language" gorm:"primary_key" valid:"required"`
	Body     string `json:"body" valid:"required"`
}

// GetTranslations returns a slice with all translations for a given owner ID
func GetTranslations(db *gorm.DB, ownerID uint) ([]Translation, error) {
	var result []Translation
	err := db.Where(&Translation{ID: ownerID}).Find(&result).Error
	return result, err
}

// GetTranslation returns the translation for the given ID and language
func GetTranslation(db *gorm.DB, ownerID uint, language string) (*Translation, error) {
	result := Translation{ID: ownerID, Language: language}
	err := db.First(&result).Error
	return &result, err
}

// SelectTranslation returns the body of the translation for the given language
func SelectTranslation(translations []Translation, language string) string {
	if len(translations) == 0 {
		return ""
	}

	for i := range translations {
		if translations[i].Language == language {
			return translations[i].Body
		}
	}
	return translations[0].Body
}

// GetLanguagesWithoutTranslation returns the list of languages that don't have
// a translation for the notification with the given ownerID
func GetLanguagesWithoutTranslation(db *gorm.DB, ownerID uint) ([]string, error) {
	supportedLanguages := SupportedLanguages(db)
	translations, err := GetTranslations(db, ownerID)
	if err != nil {
		return nil, err
	}

	result := make([]string, 0, len(supportedLanguages)-len(translations))
Languages:
	for _, lang := range supportedLanguages {
		for i := range translations {
			if translations[i].Language == lang {
				continue Languages
			}
		}
		result = append(result, lang)
	}

	return result, nil
}
