# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [NextRelease] - ReleaseDate
### Added
- 

### Changed
- 

## [4.1.3] 2018.12.19
### Changed
- add possibility to return additional information in an error using the api.DetailedError struct

## [4.0.2] 2018.04.03
### Added
- code to handle all DB connections being dropped from the pool.

## [4.0.1] 2018.03.14
### Changed
- fixed bug: calling IsSet on a nil time pointer caused invalid memory address.

## [4.0.0] 2018.02.20
### Removed
- the `/ui` endpoint, as it is now handled by the ingress.

### Changed
- added check that the apidoc and CHANGELOG files exist in function NewRouter, with panic if that's not the case.

## [3.0.0] 2018.01.19
### Added
- support for performing DB initialization for testing: the function NewRouter requires a parameter.
- renamed function DbHandler in package db to Handler, as suggested by golint

## [2.14.3] 2017.12.12
### Changed
- added html body to the mailgun e-mail

## [2.14.2] 2017.12.12
### Changed
- updated dependency to golang 1.9

## [2.14.1] 2017.12.07
### Changed
- changed behavior of function BuildJwtToken to not add the ExpiresAt field, if the tokenLifetime is 0

## [2.14.0] 2017.11.16
### Added
- function ParsePaginationRequest
- unit test for ParsePaginationRequest

### Changed
- fixed bug: the `total` query parameter wasn't parsed in function GetPaginationRequest

## [2.13.0] 2017.11.10
### Added
- function HandleError for wrapping different kind of errors to standard HTTP response codes
- function GeneratePassword for generating random passwords

## [2.12.0] 2017.11.01
### Added
- function env.GetIntFromEnv

## [2.11.0] 2017.09.14
### Added
- function GetSliceFromQuery for extracting multiple values from the query of an HTTP request.

## [2.10.0] 2017.08.15
### Added
- function StartServer

## [2.9.1] 2017.07.13
### Added
- support for the /ui endpoint in the router

### Changed
- fix for JIRA GOL-2: removed workaround for k8s libraries not ported to dependency.

## [2.9.0] 2017.06.21
### Added
- functions Atouint and GetUintFromURL

## [2.8.0] 2017.06.19
### Added
- added function Min(int, int)

### Changed
- removed input argument length from GetStartIndex and GetStopIndex

## [2.7.1] 2017.06.19
### Changed
- fixed bug in function GetStartIndex: wrong value would be returned if Skip was larger than the length of the slice to be paginated

## [2.7.0] 2017.06.19
### Added
- functions GetStartIndex and GetStopIndex for Pagination objects.

## [2.6.0] 2017.06.01
### Added
- function GetRequestProtocol

## [2.5.0] 2017.05.19
### Added
- function GetBaseURL for returning the URL from a given HTTP request

## [2.4.0] 2017.05.15
### Added
- added methods DecodeAndValidateJSON and DecodeJSON

## [2.3.1] 2017.05.15
### Removed
- JIRA GOL-7: removed the ACCESS_TOKEN environment variable

## [2.3.0] 2017.05.11
### Added

### Changed
- replaced api.Date with time.Time and added support for serialization to JSON and SQL

## [2.2.3] - 2017-04-25
### Added
- function GetBoolFromQuery: returns a boolean value from the given query parameter

### Changed
- fix for JIRA CM-6: listOwnUserCoupons supports query parameter validOnly

## [2.2.2] - 2017-04-24
### Changed
- fix for JIRA DM-15

## [2.2.1] - 2017-04-14
### Changed
- Removed required annotation for the ACCESS_TOKEN environment variable.

## [2.0.0] - 2017-01-13
### Changed
- Removed function ExtractScopes
- Fixed bug JIRA DP-120: nil pointer error when invalid JWT token provided

### Added
- ExtractScopes function in api package
- GetBoolFromEnv function in api package

## [1.0.1] - 2017-01-13
### Added
- InternalServerError function in api package
- CHANGELOG.md (this file)
