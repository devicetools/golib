package rand

import (
	"fmt"
	"os"
	"path"
	"path/filepath"

	"github.com/dchest/uniuri"
)

// GenerateID generates a good URI id (not guaranteed to be unique) of the given
// length
func GenerateID(length int) string {
	return uniuri.NewLen(length)
}

// GeneratePassword generates a password of given length
func GeneratePassword(length int) string {
	chars := uniuri.StdChars
	chars = append(chars, []byte("~!@#%^()_")...)

	return uniuri.NewLenChars(length, chars)
}

// GenerateFileName generates a new file name, with the given extension for using
// in the provided directory. The file name is guaranteed to be unique, and
// includes the given directory path. In case it fails to generate a new unique
// file name, the function returns and empty string and an appropriate error
// message
func GenerateFileName(dirName, ext string, length int) (string, error) {
	// The number of attempts at generating a new name
	n := 50

	// Check that the directory exists
	if fileInfo, err := os.Stat(dirName); err != nil {
		return ``, err
	} else if !fileInfo.IsDir() {
		return ``, fmt.Errorf(`Directory %v does not exist`, dirName)
	}

	// Glob all files from the directory, that match the given extension
	allFiles, err := filepath.Glob(path.Join(dirName, `*`+ext))
	if err != nil {
		return ``, err
	}

	// Prefix the extension with a separator, if it doesn't have one already
	if len(ext) > 0 && ext[0] != '.' {
		ext = `.` + ext
	}

	// Try to generate a new unique file name
	for i := 0; i < n; i++ {
		newName := path.Join(dirName, GenerateID(length)+ext)
		// Check that a file with that name doesnt already exist
		exists := false
		for _, f := range allFiles {
			if newName == f {
				exists = true
				break
			}
		}
		if !exists {
			return newName, nil
		}
	}

	return ``, fmt.Errorf("Couldn't generate a unique file name after %v tries ", n)
}
