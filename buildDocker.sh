#!/bin/bash


#----------------------------------------------
printUsage()
{
cat << %%USAGE%%
     Usage: $(basename ${0}) -h
            $(basename ${0}) -t tag

    Description:
        Builds a new golib docker image.

    Options:
       -h
       --help
             Print this help message and exit.

       -t tag
	      Tags the image with the given tag (default: ${tag}).

       -s
	      Run in silent mode, printing less information.
%%USAGE%%
}


#----------------------------------------------
# Main script
#----------------------------------------------
shortOptions='ht:s'  # Add short options here
longOptions='help,listOptions' # Add long options here
if $(isMac); then
    ARGS=`getopt "${shortOptions}" $*`
else
    ARGS=$(getopt -u -o "${shortOptions}" -l "${longOptions}" -n "$(basename ${0})" -- "$@")

fi

# If wrong arguments, print usage and exit
if [[ $? -ne 0 ]]; then
    printUsage
    exit 1;
fi

eval set -- "$ARGS"

tag=local
silent=false
while true; do
    case ${1} in
    --listOptions)
        echo '--'$(sed 's/,/ --/g' <<< ${longOptions}) $(echo ${shortOptions} |
            sed 's/[^:]/-& /g') | sed 's/://g'
        exit 0
        ;;
    -h|--help)
        printUsage
        exit 0
        ;;
    -t)
        tag=${2}
        shift 2
        ;;
    -s)
        silent=true
        shift
        ;;
    --)
        shift
        break
        ;;
    "")
        # This is necessary for processing missing optional arguments
        shift
        ;;
    esac
done

# Uncomment this for enabling debugging
# set -x

# if [[ -z ${tag} ]] && [[ -f .tag ]]; then
#     tag=$(cat .tag)
# fi

if [[ ${silent} != true ]]; then
    printf "\nBuilding devicetools/golib:%s\n\n" ${tag}
fi

docker build . -t devicetools/golib:${tag}
