package db

import (
	"net/http"

	"bitbucket.org/devicetools/golib/api"

	"github.com/jinzhu/gorm"
)

// Handler a HTTP handler with access to a database connection
type Handler func(*gorm.DB, http.ResponseWriter, *http.Request) error

// WrapDbHandler wrap a DbHandler in an ErrorHandler and pass along the database
// to return a plain HTTP handler that we can use to register to normal API methods
func WrapDbHandler(db *gorm.DB, h Handler) http.HandlerFunc {
	return api.ErrorHandler(
		func(w http.ResponseWriter, r *http.Request) error {
			return h(db, w, r)
		})
}
