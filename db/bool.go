package db

import (
	"database/sql/driver"
	"fmt"
	"strings"
)

// Bool boolean type for using with db/sql
type Bool bool

// FromString constructs a Bool from a string
func FromString(str string) (Bool, error) {
	switch strings.ToLower(str) {
	case `0`, `false`:
		return Bool(false), nil
	case `1`, `true`:
		return Bool(true), nil
	}

	return Bool(false), fmt.Errorf("Cannot convert %v to Bool", str)
}

// FromInt64 constructs a Bool from an int64 value
func FromInt64(val int64) (Bool, error) {
	switch val {
	case 0:
		return Bool(false), nil
	case 1:
		return Bool(true), nil
	}

	return Bool(false), fmt.Errorf("Cannot convert %v to Bool", val)
}

// String returns the string representation for Bool
func (b Bool) String() string {
	if b {
		return `1`
	}
	return `0`
}

// Scan implements the sql interface
func (b *Bool) Scan(value interface{}) error {
	// Seems like the mysql driver returns type []uint8 when the table is
	// empty (or the Bool column is null) and type int64 when the column
	// has non-null values - so we must handle both cases
	var err error
	if uintVal, ok := value.(int64); ok {
		*b, err = FromInt64(uintVal)
		return err
	}

	*b, err = FromString(string(value.([]uint8)))
	return err
}

// Value implements sql interface
func (b Bool) Value() (driver.Value, error) {
	return b.String(), nil
}
