package db

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	_ "github.com/jinzhu/gorm/dialects/mysql"

	"github.com/DavidHuie/gomigrate"
	"github.com/jinzhu/gorm"
	"github.com/kelseyhightower/envconfig"
)

// dbConfig defines the configuration used for connecting to the DB
type dbConfig struct {
	Host     string
	Name     string
	User     string
	Password string
	Port     int `default:"3306"`
	Migrate  bool
}

const (
	migrationDir = `./db/migrations`
)

// connectionString returns the string to be used when connecting to the DB
func (c dbConfig) connectionString() string {
	// Without parsing time the timestamp would be mapped to []uint8
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true", c.User, c.Password, c.Host, c.Port, c.Name)
}

// Migrate performs DB migration, based on the files in ./db/migrations directory
func Migrate(db *sql.DB) error {
	migrator, err := gomigrate.NewMigrator(db, gomigrate.Mysql{}, migrationDir)
	if err != nil {
		return err
	}

	err = migrator.Migrate()
	return err
}

// Initialize initializes the DB with data from the initialize.sql file
func Initialize(db *sql.DB) (err error) {
	// Check that the initialization file exists
	fileName := filepath.Join(migrationDir, `initialize.sql`)
	if _, err = os.Stat(fileName); err != nil {
		return
	}

	// Get the list of all tables in the DB
	rows, err := db.Query(`SHOW TABLES;`)
	if err != nil {
		return
	}
	defer rows.Close()

	// Execute the initialization as a transaction
	transaction, err := db.Begin()
	if err != nil {
		return
	}

	// Function for handling transaction errors
	handleTransactionError := func(input error) error {
		if rollbackError := transaction.Rollback(); rollbackError != nil {
			return rollbackError
		}
		return input
	}

	// Disable the foreign key checks
	if _, err = transaction.Exec(`SET FOREIGN_KEY_CHECKS=0;`); err != nil {
		return handleTransactionError(err)
	}

	// Delete all the data in all tables, except for the gomigrate table
	table := ``
	for rows.Next() {
		// Get the table's name...
		if err = rows.Scan(&table); err != nil {
			return
		}

		// ... ignore gomigrate
		// TODO: fork the gomigrate repo and expose the gomigrate table name
		if table == `gomigrate` {
			continue
		}

		// ... and delete all the data in it
		if _, err = transaction.Exec(fmt.Sprintf("DELETE FROM %v", table)); err != nil {
			return handleTransactionError(err)
		}
	}

	// Enable the foreign key checks
	if _, err = transaction.Exec(`SET FOREIGN_KEY_CHECKS=1;`); err != nil {
		return handleTransactionError(err)
	}

	// Get the initialization SQL statements from the file
	sql, err := ioutil.ReadFile(fileName)
	if err != nil {
		return fmt.Errorf("Error reading migration: %s", fileName)
	}

	commands := gomigrate.Mysql{}.GetMigrationCommands(string(sql))
	for _, cmd := range commands {
		_, err = transaction.Exec(cmd)
		if err != nil {
			return handleTransactionError(err)
		}
	}

	return transaction.Commit()
}

// FromConfig returns a pointer to a gorm DB, created by using the dbConfig values
// read from the environment. If mandatory dbConfig fields are missing in the environment
// the function throws a fatal error.
func FromConfig() *gorm.DB {
	var cfg dbConfig
	err := envconfig.Process("db", &cfg)
	if err != nil {
		log.Fatal(err)
	}

	// Try to connect to the DB
	var db *gorm.DB
	retryCount := 10
	timeout := 5
	for index := 0; index < retryCount; index++ {
		db, err = gorm.Open("mysql", cfg.connectionString())
		if err != nil {
			fmt.Printf("%v. Failed to connect to DB. Waiting %v seconds.\n", index+1, timeout)
			fmt.Printf("\t(error was: %v)\n", err)
			time.Sleep(time.Duration(timeout) * time.Second)
		}
	}

	// If failed to connect, panic
	if err != nil {
		log.Fatal(err)
	}

	// This should prevent the dropping of connections to the DB
	db.DB().SetMaxIdleConns(0)
	db.DB().SetMaxOpenConns(100)

	// Migrate, if requested
	if cfg.Migrate {
		err = Migrate(db.DB())
		if err != nil {
			log.Fatal(err)
		}
	}

	return db
}
