package db

import (
	"strings"

	"bitbucket.org/devicetools/golib/api"
	"github.com/jinzhu/gorm"
)

// HandleError handles the error, returning specialized ones in cases like RecordNotFound
func HandleError(err error) error {
	switch {
	// no error ==> do nothing
	case err == nil:
		return nil
	// gorm.ErrRecordNotFound ==> api.NotFoundError
	case err == gorm.ErrRecordNotFound:
		return api.NotFoundError(err.Error())
	// MySQL Error 1062 Duplicate entry ==> NotAcceptableError
	case strings.Contains(err.Error(), `Error 1062`):
		return api.NotAcceptableError(err.Error())
	// otherwise, return the error as it is
	default:
		return err
	}
}
