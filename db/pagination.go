package db

import (
	"bitbucket.org/devicetools/golib/commons"
	"github.com/jinzhu/gorm"
)

// Paginate returns a copy of the DB with pagination
func Paginate(db *gorm.DB, pagination commons.Pagination) *gorm.DB {
	return db.Offset(pagination.Skip).Limit(pagination.Limit)
}
