package crypto

import "golang.org/x/crypto/bcrypt"

const bcryptCostFactor = 12

// HashPassword takes a string, salts and hashes it using bcrypt with a cost factor of 12
func HashPassword(password string) (string, error) {
	// Hash the user password with bcrypt
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcryptCostFactor)
	if err != nil {
		return "", err
	}

	return string(hash), nil
}

// ValidatePassword takes a hashed and plaintext string and compares if the hashing the plain text equals the hash
func ValidatePassword(hashed string, plainText string) (bool, error) {
	hashBytes := []byte(hashed)
	plainBytes := []byte(plainText)

	if err := bcrypt.CompareHashAndPassword(hashBytes, plainBytes); err != nil {
		return false, err
	}

	return true, nil
}