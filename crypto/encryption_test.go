package crypto

import (
	"testing"
	"bitbucket.org/devicetools/usermanagement/backend/crypto"
)

func TestEncryptAndDecrypt(t *testing.T) {
	tests := []struct {
		name      string
		key       string
		plainText string
	}{
		{"Encryption Test 1", "4msdsGKtkHsGK2XDgS6MN0JGORUAPw2b", "Text"},
		{"Encryption Test 2", "olg1Y3JnoSUdsLrVdM92DAvgx5518y7a", "Some random content"},
		{"Encryption Test 3", "2fxzzCY05HRq8i21jVOVAi2kBQGV3eNu", "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			key := []byte(tt.key)
			cipher, err := crypto.Encrypt([]byte(tt.plainText), key, ``)
			if err != nil {
				t.Errorf("Encryption error: %v", err)
				return
			}

			plain, err := crypto.Decrypt(cipher, key)
			if err != nil {
				t.Errorf("Decryption error: %v", err)
				return
			}

			plainStr := string(plain)
			if plainStr != tt.plainText {
				t.Errorf("Decryption plain: %v, expected: %v", plainStr, tt.plainText)
				return
			}
		})
	}
}

func TestEncryptAndDecryptString(t *testing.T) {
	tests := []struct {
		name      string
		key       string
		plainText string
	}{
		{"Encryption Test 1", "4msdsGKtkHsGK2XDgS6MN0JGORUAPw2b", "Text"},
		{"Encryption Test 2", "olg1Y3JnoSUdsLrVdM92DAvgx5518y7a", "Some random content"},
		{"Encryption Test 3", "2fxzzCY05HRq8i21jVOVAi2kBQGV3eNu", "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cipher, err := crypto.EncryptString(tt.plainText, tt.key, ``)
			if err != nil {
				t.Errorf("Encryption error: %v", err)
				return
			}

			plain, err := crypto.DecryptString(cipher, tt.key)
			if err != nil {
				t.Errorf("Decryption error: %v", err)
				return
			}

			plainStr := plain
			if plainStr != tt.plainText {
				t.Errorf("Decryption plain: %v, expected: %v", plainStr, tt.plainText)
				return
			}
		})
	}
}

func TestEncryptAndDecryptReproducible(t *testing.T) {
	key := "CDRJC2kq2uUGrBvwoGscaTBZ324rCRKj"
	plainText := "Never gonna give you up"
	iv := "123456789012"

	first, err := crypto.EncryptString(plainText, key, iv)
	if err != nil {
		t.Errorf("Encryption error: %v", err)
	}

	second, err := crypto.EncryptString(plainText, key, iv)
	if err != nil {
		t.Errorf("Encryption error: %v", err)
	}

	if first != second {
		t.Error("Encryption did not produce reproducible results")
	}
}