package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"errors"
	"encoding/base64"
	"fmt"
	"io"
	"crypto/rand"
	"strings"
)


// EncryptString is a convenience method that takes parameters as strings instead of byte slices and returns a string
func EncryptString(plainText string, key string, iv string) (string, error) {
	cipherText, err := Encrypt([]byte(plainText), []byte(key), iv)
	return string(cipherText), err
}

// Encrypt encrypts the given plain text with the given key using AES in GCM operation mode.
// The key length is used to determine the block cipher to be used: 16 = AES-128, 24 = AES-192, 32 = AES-256
func Encrypt(plainText []byte, key []byte, iv string) ([]byte, error) {
	// Create new AES block cipher based on key length
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// Create a new GCM operation mode for the block cipher
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	// Create a nonce for the GCM
	nonce := make([]byte, gcm.NonceSize())

	// If configured to create a non-reproducible encryption, create a random nonce
	if iv != `` {
		if _, err = io.ReadFull(strings.NewReader(iv), nonce); err != nil {
			return nil, err
		}
	} else {
		if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
			return nil, err
		}
	}

	// Encrypt and return
	cipherText := gcm.Seal(nil, nonce, plainText, nil)
	cipherText = append(nonce, cipherText...)

	base64Cipher := make([]byte, base64.RawStdEncoding.EncodedLen(len(cipherText)))
	base64.RawStdEncoding.Encode(base64Cipher, cipherText)

	return base64Cipher, nil
}

// DecryptString is a convenience method that takes parameters as strings instead of byte slices and returns a string
func DecryptString(cipherText string, key string) (string, error) {
	plainText, err := Decrypt([]byte(cipherText), []byte(key))
	return string(plainText), err
}

// Decrypt decrypts the given plain text with the given key using AES in GCM operation mode
func Decrypt(cipherText []byte, key []byte) ([]byte, error) {
	// Create new AES block cipher based on key length
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// Create a new GCM operation mode for the block cipher
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	// Check if the cipher text length is at least the GCM nonce size
	nonceSize := gcm.NonceSize()
	if len(cipherText) < nonceSize {
		return nil, errors.New(fmt.Sprintf("ciphertext too short. actual: %d, expected at least: %d", len(cipherText), nonceSize))
	}

	// Decode the cipher text
	decoded := make([]byte, base64.RawStdEncoding.DecodedLen(len(cipherText)))
	_, err = base64.RawStdEncoding.Decode(decoded, cipherText)
	if err != nil {
		return nil, err
	}

	// Split cipher text into nonce and the actual cipher text and decrypt it
	nonce, ciphertext := decoded[:nonceSize], decoded[nonceSize:]
	decrypted, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}

	return decrypted, nil
}
