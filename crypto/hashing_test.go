package crypto

import (
	"testing"
	"bitbucket.org/devicetools/usermanagement/backend/crypto"
)

func TestHashAndValidatePassword(t *testing.T) {
	password := `NOT_SO_SECRET`
	hash, err := crypto.HashPassword(password)
	if err != nil {
		t.Errorf(`Hashing password error: %v`, err)
	}

	if hash == password {
		t.Error("Password and hash should not be the same")
	}

	valid, err := crypto.ValidatePassword(hash, password)
	if err != nil {
		t.Errorf("Validating password error: %v", err)
	}

	if !valid {
		t.Errorf("Validating password with hash returned false")
	}
}