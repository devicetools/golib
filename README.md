# golib

Contains higher level abstractions for Go separated as packages.
Specifically it includes basic building blocks for

- `cli`: CLI interfaces
- `api`: HTTP APIs

## Integrate Rollbar Golang Error Tracking

Rollbar allows error tracking within application to track when something has gone wrong
and why. It can be configured via the `ROLLBAR_TOKEN` and optional `ROLLBAR_ENVIRONMENT` token.

In your `main` function you need to initialize the agent.

```golang
apilib.InitRollbarFromConfig()
defer rollbar.Wait()
```

And then set it up as HTTP middleware.

```
router = api.RollbarRecoveryHandler(router)
```

## Integrate New Relic Golang Plugin

Via the [Gorelic Plugin](https://github.com/yvasiyarov/gorelic) we collect performance metrics about the Golang runtime in New Relic.

After you've initialized the Gorelic agent you can use it as HTTP middleware.

```
agent := api.NewRelicAgentFromConfig()
r = agent.TrackRequest(r)
```
