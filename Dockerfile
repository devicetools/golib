FROM golang:1.9

WORKDIR /go/src/bitbucket.org/devicetools/golib
COPY . .

RUN ./build.sh
